/**
* @file                 api_type.h
* @brief                结构体类型
* @details		
* @author               stopfan
* @date         2013-11-11
* @version      A001
* @par Copyright (c): 
*               新动力科技
* @par History:         
*       version: stopfan, 2013-12-12, desc\n
*/

#ifndef __api_type_h__
#define __api_type_h__

#define EIResult_t int

typedef enum {
	GSMGPRS_DEFMODE = 0,
	GSMGPRS_ATMODEM,
	GMSGPRS_DTUMODEM,
}MODEM_TYPE;

typedef enum {
	EASYIO_SERVICE = 0,
	CUSTOME_SERVICE,
}CLOUD_TYPE;

struct PlatInterface {
	void (*ResetModem)(void);
	void (*PowerModem)(void);
	char ATCommandUartPortName[8];
	char FirstUartPortName[8];
	char SecondUartPortName[8];
	void (*Empty)(void);
};

struct ModemOption {
	MODEM_TYPE type;
};

struct GprsNetworkOption {
	char apn[32];
	char enable;
};

struct TcpcliOption {
	char host[64];				/**< 目的服务器IP或域名	*/
	char port[8];				/**< 目的服务器端口	*/
	int heartbeat_cycle;			/**< 心跳周期，单位秒	*/
	unsigned char login_buffer[32];		/**< 注册包，链接至TCP发送的第一个数据包	*/
	int login_buffer_len;
	unsigned char heartbeat_buffer[32];	/**< 心跳内容		*/
	int heartbeat_len;			/**< 心跳内容长度	*/
//private:
	char enable;
};

struct CloudOption {

	CLOUD_TYPE type;
	char password[32];				/**< EasyIO 设备密码	*/

	char cus_username[32];				/**< 即时通讯服务器用户名	*/
	char cus_password[32];				/**< 即时通讯服务器用户密码	*/
	char cus_domain[64];				/**< 即时通讯服务器域		*/
	char cus_port[8];					/**< 即时通讯服务器端口		*/
	char cus_host[64];					/**< 即时通讯服务器主机地址	*/

//private:
	char enable;
};



struct SysInfo {
	unsigned int uptime;				/**< 模块运行时间，单位：秒	*/
	unsigned int totalmem;				/**< 动态内存总量		*/
	unsigned int freemem;				/**< 可用内存总量		*/
	unsigned int usemem;				/**< 已用内存			*/
};

struct GsmLocInfo {
	char longitude[16];				/**< 基站的经度信息		*/
	char latitude[16];				/**< 基站的纬度信息		*/
	char date[16];					/**< 当前日期UTC		*/
	char time[16];					/**< 当前时间UTC		*/
};

struct NetworkOption {
	char Contype [16];				/**< 网络参数 一般为GPRS	*/
	char APN [16];					/**< APN 名称 一般为CMNET	*/
};

typedef enum {
	SYSEVENT_TCP_DISCONNECT,		/**< TCP连接断开事件	*/
	SYSEVENT_TCP_CONNECTED,			/**< TCP连接成功事件	*/
	SYSEVENT_XMPP_CONNECTED,		/**< XMPP连接成功事件	*/
	SYSEVENT_XMPP_LOGIN,			/**< XMPP连接成功事件	*/
	SYSEVENT_XMPP_LOGGED,			/**< XMPP服务器连接成功 */
	SYSEVENT_XMPP_DISCONNECT,		/**< XMPP连接断开事件	*/

	RECVEVENT_SMS,				/**< 收到短消息息事件	*/
	RECVEVENT_SERIAL,			/**< 收到串口数据	*/
	RECVEVENT_TCP,				/**< 收到DTU TCP 数据	*/
	RECVEVENT_HTTPRESP,			/**< 收到HTTP请求返回	*/
	RECVEVENT_XMPPMSG,			/**< 收到XMPP消息	*/
	RECVEVENT_CLOUDRESP,			/**< 收到云服务请求结果	*/


	SYSEVENT_NULL
}EI_SYS_EVENT;

typedef enum {
	EI_ADC0,
	EI_ADC1,
	EI_ADC_NULL
}ADC_PORT;

typedef enum {
	USART_PORT_1 = 0,
	USART_PORT_2,
}USART_PORT_NUM;



struct RcvSmsEvent {
	char *num;
	char *body;
};

struct RcvSerialDataEvent {
	USART_PORT_NUM port;
	unsigned char *data;
	unsigned int len;
};

struct RcvTcpDataEvent {
	unsigned char *data;
	unsigned int len;
};

struct RcvHttpRespEvent {
	unsigned char *data;
	unsigned int len;
};


struct RcvXmppMsgEvent {
	char *from;
	char *body;
};

struct RcvCloudRespEvent {
	unsigned char *data;
	unsigned int len;
};

#endif
