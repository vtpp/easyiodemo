#include "api/api.h"
#include "api/init.h"

#include <string.h>
#include <stdio.h>


const char REMOTE_HOST[] = "liud-home.f3322.org";		// 目的IP
const char REMOTE_PORT[] = "5000";									// 目的端口

const char HB_DATA[] = {0x0};									//心跳数据
const int HB_DATA_TIME = 60;									//心跳间隔

#define DTU_SERIAL_PORT USART_PORT_1 //使用串口1作为数据传输口

static void recv_xmpp(char *from , char *msg)
{
}

static void recv_sms(char *num , char *body)
{
}

static void recv_tcp_data(unsigned char * buffer , unsigned int size)
{
	ApiWriteSerial(USART_PORT_1,buffer,size);
}

static void press_serial_data(unsigned char data , unsigned int len)
{
}

static void recv_serial_data(USART_PORT_NUM port,unsigned char *buffer , unsigned int size)
{
	if (port == DTU_SERIAL_PORT)
	{
		ApiSendTcpData(buffer,size);
	}
	
	if (size == 3)
	{
		if (strstr(buffer,"+++"))
		{
			char __tmp = 0xEE;
			ApiWriteEEPROM(0,&__tmp,1);
			ApiSetSysDebugMask(0xffffffff);
		}
		else if (strstr(buffer,"---"))
		{
			char __tmp = 0x00;
			ApiWriteEEPROM(0,&__tmp,1);
			ApiSetSysDebugMask(0x0);
		}
	}

}

void  StartupImpl(void)
{
	unsigned char tmp;
	struct GprsNetworkOption gprs_opt;
	struct TcpcliOption tcp_opt;

	ApiReadEEPROM(0,&tmp,1);
	if (tmp == 0xEE)
		ApiSetSysDebugMask(0xffffffff);
	
	ApiEnableWatchDog(); //打开看门狗
  snprintf(gprs_opt.apn,sizeof(gprs_opt.apn),"%s","CMNET");
  InitGprsNetwork(&gprs_opt); //初始化GPRS网络

	snprintf(tcp_opt.host,sizeof(tcp_opt.host),"%s",REMOTE_HOST);
	snprintf(tcp_opt.port,sizeof(tcp_opt.port),"%s",REMOTE_PORT);
	
	memcpy(tcp_opt.heartbeat_buffer,HB_DATA,sizeof(HB_DATA));
	
	tcp_opt.heartbeat_cycle = HB_DATA_TIME; //心跳间隔
	tcp_opt.heartbeat_len = sizeof(HB_DATA); //心跳包长度
	
	InitTcpcli(&tcp_opt);
	
	ApiSetSysDebugMask(0xffffffff);


}
void RecvSysEventImpl(EI_SYS_EVENT event , void *body)
{

	switch(event)
	{
		case RECVEVENT_SMS: //收到SMS信息
			recv_sms(
					((struct RcvSmsEvent*)body)->num,
					((struct RcvSmsEvent*)body)->body
				);
			break;
		case RECVEVENT_TCP: //收到TCP数据
			recv_tcp_data(
					((struct RcvTcpDataEvent*)body)->data,
					((struct RcvTcpDataEvent*)body)->len
				     );
			break;
		case RECVEVENT_SERIAL: //串口有信息到来
			recv_serial_data(
					((struct RcvSerialDataEvent*)body)->port,
					((struct RcvSerialDataEvent*)body)->data,
					((struct RcvSerialDataEvent*)body)->len
					);
			break;
		case RECVEVENT_XMPPMSG:
			recv_xmpp(
					((struct RcvXmppMsgEvent*)body)->from,
					((struct RcvXmppMsgEvent*)body)->body
				 );
			break;
		case SYSEVENT_TCP_CONNECTED:
			//ApiSendTcpData(LOGIN_DATA,sizeof(LOGIN_DATA));
			break;
		default:
			break;
	}
	//
}

